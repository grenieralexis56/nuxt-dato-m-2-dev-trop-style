export const getGradeColorFrom0To20 = (grade: number) => {
  if (grade < 5) {
    return "red-500";
  } else if (grade < 10) {
    return "orange-500";
  } else if (grade < 15) {
    return "yellow-500";
  } else if (grade < 20) {
    return "green-500 text-bold";
  } else {
    return "blue-500";
  }
};

export const ellipsisString = (chaine: string): string => {
  if (chaine.length > 135) {
    return chaine.slice(0, 135) + "...";
  } else {
    return chaine;
  }
};
