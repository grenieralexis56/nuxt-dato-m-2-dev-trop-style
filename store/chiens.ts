// store/filters.ts
import { defineStore } from "pinia";

export const useChiensStore = defineStore("chiensStore", {
  state: () => ({
    /** @type {{ id: string, name: string, note: number }[]} */
    chiens: [],
    filter: "all",
    nextId: 0,
  }),
  actions: {
    setChiens(chiens: never[]) {
      this.chiens = chiens;
    },
  },
  getters: {
    getChiens(): never[] {
      return this.chiens;
    },
    highRatedChiens(): never[] {
      return this.chiens.filter(
        (chien: { id: string; name: string; note: number }) => chien.note >= 17
      );
    },
    lowRatedChiens(): never[] {
      return this.chiens.filter(
        (chien: { id: string; name: string; note: number }) => chien.note <= 10
      );
    },
  },
});
