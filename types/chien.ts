export interface Chien {
  name: string;
  photos: Photo[];
  description: string;
  note: number;
  slug: string;
}

interface Photo {
  url: string;
}
