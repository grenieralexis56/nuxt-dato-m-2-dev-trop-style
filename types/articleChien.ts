import { Chien } from "./chien";

export interface articleChien {
  title: string;
  slug: string;
  description: string;
  chiens: Chien[];
}
