export default gql`
  query ArticleChien {
    allArticleChiens {
      title
      id
      description
      slug
      chiens {
        slug
        name
        note
        photos {
          url
        }
      }
      _createdAt
      _seoMetaTags {
        content
        attributes
      }
      _status
    }
  }
`;
