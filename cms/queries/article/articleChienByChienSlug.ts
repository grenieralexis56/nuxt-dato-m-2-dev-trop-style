// gql query to get a all articles referring to a dog by its slug
export default gql`
  query ArticlesChienBySlugChien($chien: Chien!) {
    allArticleChiens(filter: { chiens: { exists: $chien } }) {
      id
      title
      slug
      description
      chiens {
        id
        name
        description
        note
        slug
        photos {
          url
        }
      }
    }
  }
`;
