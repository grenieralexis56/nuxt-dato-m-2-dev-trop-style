// gql query to get a single article by slug
export default gql`
  query Article($slug: String!) {
    articleChien(filter: { slug: { eq: $slug } }) {
      id
      description
      slug
      title
      chiens {
        slug
        name
        photos {
          url
        }
      }
      _createdAt
      _seoMetaTags {
        attributes
        content
        tag
      }
    }
  }
`;
