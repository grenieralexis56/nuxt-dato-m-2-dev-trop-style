// gql query to get a single chien by id
export default gql`
  query Chien($slug: String!) {
    chien(filter: { slug: { eq: $slug } }) {
      id
      name
      description
      note
      slug
      photos {
        url
      }
      _seoMetaTags {
        attributes
        content
        tag
      }
    }
  }
`;
