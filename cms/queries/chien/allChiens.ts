export default gql`
  query AllChiens {
    allChiens {
      id
      name
      description
      note
      slug
      photos {
        url
      }
      _seoMetaTags {
        attributes
        content
        tag
      }
    }
  }
`;
