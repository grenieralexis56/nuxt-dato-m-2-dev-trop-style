// gql query to get all chiens with note === 20
export default gql`
  query Chien {
    allChiens(filter: { note: 20 }) {
      id
      name
      description
      note
      slug
      photos {
        url
      }
    }
  }
`;
